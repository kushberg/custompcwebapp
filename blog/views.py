from django.shortcuts import render
from .models import Post

post = [{'author': 'Sebastian Swoboda',
         'title': 'Blog Post 1',
         'content': 'Hello Guys',
         'date_posted': '07/06/2019'
         }]


# Create your views here.
def home(request):
    context = {'posts': post}
    return render(request, 'blog/home.html', context)


def about(request):
    return render(request, 'blog/home.html', {'title': 'About'})
